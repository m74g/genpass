;(function(){
    const upperCaseLetters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const lowerCaseLetters = 'abcdefghijklmnopqrstuvwxyz';
    const numbers          = '0123456789';
    const symbols          = " !\"#$%&'()*+,-./:;<=>?@[\]^_`{|}~";
    const btn              = document.querySelector('.generate');
    const resultInput      = document.querySelector('.result');
    let passWordChars  = '';
    let passWordLength = 16;
    let passWordConfig = {
            alphabet   : document.querySelector('.alphabet'),
            numbers    : document.querySelector('.numbers'),
            symbols    : document.querySelector('.symbols'),
            isUpperCase: document.querySelector('.isuppercase')
    }

    let validateForm = () => {
        passWordChars = '';
        let count = 0;

        for( let i in passWordConfig ) {            
            if(!passWordConfig[i].checked) {
                count += 1;
            }
        }   
        
        passWordConfig.alphabet.checked ? passWordChars += lowerCaseLetters : '';
        passWordConfig.numbers.checked ? passWordChars += numbers : '';
        passWordConfig.symbols.checked ? passWordChars += symbols : '';
        passWordConfig.isUpperCase.checked ? passWordChars += upperCaseLetters : '';

        if (count > 3) {
            return false;
        } else {
            return true;
        }
    };

    let getRandomValue = array => {
        return array[Math.floor(Math.random() * (+array.length - +0)) + +0]
    }

    let generatePassword = () => {
        if (validateForm()) {
            let pass = '';
            for(let i = 0; i <= passWordLength; i++) {
                pass += getRandomValue(passWordChars);
            }  

            resultInput.innerHTML = pass;       
        } else {
            alert('Form validate error. You should choose minimum 1 checkbox!')
            return;
        }
    };

    btn.addEventListener('click', generatePassword)
}());